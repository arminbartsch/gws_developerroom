config.tx_extbase.persistence.classes {
    GeorgRinger\News\Domain\Model\News {
        subclasses {
            3 = Developerroom\GwsDeveloperroom\Domain\Model\NewsGallery
        }
    }

    Developerroom\GwsDeveloperroom\Domain\Model\NewsGallery {
        mapping {
            recordType = 3
            tableName = tx_news_domain_model_news
        }
    }
}


