<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('gws_developerroom', 'Configuration/TypoScript', 'Developer Grundschule Wüsten');

$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['type']['config']['items']['3'] =
    ['News Gallery', 3, 'EXT:gws_developerroom/Resources/Public/Icons/news_domain_model_news_external.png'];

$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['tags'] = array(
    'exclude' => 1,
    'l10n_mode' => 'mergeIfNotBlank',
    'label' => 'Tags',
    'config' => array(
        'type' => 'select',
        'renderType' => 'selectMultipleSideBySide',
        'MM' => 'tx_news_domain_model_news_tag_mm',
        'foreign_table' => 'tx_news_domain_model_tag',
        'foreign_table_where' => 'ORDER BY tx_news_domain_model_tag.title',
        'size' => 10,
        'minitems' => 1,
        /*'authMode' => 'explicitAllow',*/
        'maxitems' => 999,


    ),
);

$GLOBALS['TCA']['tx_news_domain_model_news']['types']['0']['showitem'] = 'hidden,l10n_parent, l10n_diffsource,
					type, title, paletteCore, fe_group,datetime,istopnews, categories,tags,teaser,
					bodytext;;;richtext::rte_transform[flag=rte_disabled|mode=ts_css],
					rte_disabled;LLL:EXT:cms/locallang_ttc.xml:rte_enabled_formlabel,tx_newsfal_image,fal_media , media, related,related_from';



$GLOBALS['TCA']['tx_news_domain_model_news']['types']['3'] = $GLOBALS['TCA']['tx_news_domain_model_news']['types']['0'];

$TCA['sys_category']['columns']['parent']['config']['maxitems'] = 200;
$TCA['sys_category']['columns']['items']['config']['maxitems'] = 200;

